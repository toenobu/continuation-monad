class Cont {
    run: Function;

    constructor(run: Function){
        this.run = run;
    }

    public bind(fn: Function) : Cont {
        var run = this.run;
        return new Cont( (fn2: Function) => run( a => fn(a).run(fn2) ) )
    }

    public bind_with_log(fn: Function) : Cont {
        var run = this.run; // fn0 => fn0(4)

        return new Cont( (fn2: Function) => run( a => {
            console.log(a);

            let c =  fn(a);
            c.run(fn2);

            console.log("");
        }) )
    }

    public static unit<T>(a: T) : Cont {
        return new Cont( (fn: Function) => fn(a))
    }

    public static callcc(fn: Function) : Cont {
        return new Cont(
            (fn2: Function) => {
                fn( a => new Cont(
                    (x: Function) => fn2(a)
                ).run(fn2))
            }
        )
    }
}

console.log("hello cont\n");

console.log("basic");
const squareCont = (x: number) => Cont.unit<number>(x * x);
squareCont(5).run(console.log)
console.log("");

console.log("bind 1");
const add4Cont = (x: number) => Cont.unit<number>(x + 4);
const minus2Cont = (x: number) => Cont.unit<number>(x - 2);
console.log("");

Cont.unit(4).bind_with_log(add4Cont).bind_with_log(minus2Cont).run(console.log)

console.log("bind 2");
const mul2Cont = (x: number) => new Cont(fn => fn(x * 2));
const minus1Cont = (x: number) => new Cont(fn => fn(x - 1));
new Cont(fn => fn(4)).bind_with_log(mul2Cont).bind_with_log(minus1Cont).run(console.log)
console.log("");

console.log("bind 3");
Cont.unit(4).bind(add4Cont).bind(squareCont).bind(minus2Cont).run(console.log)
console.log("");

// looks not working... 
console.log("callcc");
const n = 3;
const add1Cont = (x: number) => {
    console.log(x);
    return Cont.unit<number>(x + 1);
}
Cont.callcc( exit => {
    Cont.unit(n)
        .bind(add1Cont)
        .bind( x => x != 0 ? Cont.unit(1.0/ x) : exit("*** error"))
        .bind(squareCont)
}).run(console.log)