console.log("sample")

let addn = (a:number) => (b:number) => a+b

console.log(addn(5)(6))

let addCont = (a:number) => (fn: Function) => fn(a)

addCont(5)( p => console.log(p * 10))