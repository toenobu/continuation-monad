# Continuation Monad 
Continuation Monad

- [Javaで継続モナド](https://terazzo.hatenadiary.org/entry/20110723/1311447111)
- [「JavaScriptでの非同期関数合成」を継続モナドで](https://terazzo.hatenadiary.org/entry/20120727/1343400811)

- [CPS(継続渡しスタイル)のプログラミングに入門する](https://qiita.com/Kai101/items/eae3a00fcd1fc87e25fb)
- [継続渡し形式（CPS](https://web.archive.org/web/20170718125434/http://www.h4.dion.ne.jp/~unkai/js/js12.html)