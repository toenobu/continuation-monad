.PHONY: deps build run

deps:
	npm install

build:
	npm run build

cont: build
	npm run cont

sample: build
	npm run sample